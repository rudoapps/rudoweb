<?php
//MENU
define("MENU_OPTION1", "SERVICES");
define("MENU_OPTION2", "PROJECTS");
define("MENU_OPTION3", "ABOUT");
define("MENU_OPTION4", "CONTACT");

//ANCHOR
define("ANCHOR_OPTION0", "home");
define("ANCHOR_OPTION1", "services");
define("ANCHOR_OPTION2", "projects");
define("ANCHOR_OPTION3", "about");
define("ANCHOR_OPTION4", "contact");

//SLIDER 1
define("SLIDER1_TITLE", "Mobile app <br/>development <br/>
	for iOS and Android");

//SLIDER 2
define("SLIDER2_TITLE", "Services");
define("SLIDER2_SUBTITLE", "We love what me do");
define("SLIDER2_TEXT", "We design and develop highly polished iPhone & Android apps for startups and enterprise clients.");

define("SLIDER2_COL1_TITLE", "Design, UI and UX");
define("SLIDER2_COL1_SUBTITLE", "Our mobile app designers are focused on usability and aesthetics.");

define("SLIDER2_COL2_TITLE", "iOS and Android development");
define("SLIDER2_COL2_SUBTITLE", "We use Java and Swift in order to build high performance and scalable mobile apps.");

define("SLIDER2_COL3_TITLE", "Backend and Cloud");
define("SLIDER2_COL3_SUBTITLE", "We have experience using robust back-end technologies, such as Django, to generate REST Api. We use Amazon Web Services servers.");

define("SLIDER2_BUTTON", "CONTACT");

//SLIDER 3
define("SLIDER3_TITLE", "Projects");
define("SLIDER3_SUBTITLE", "We are rude, but our results are impeccable.");

//WORK1
define("WORK1_TITLE", "A marketplace to buy and sell second hand bikes");

//WORK2
define("WORK2_TITLE", "Get closer to your idols. <br/>Get money with your followers");

//WORK3
define("WORK3_TITLE", "Do not run out of energy. Rent your mobile charger everywhere.");

define("WORK4_TITLE", "Exclusive social network for ambitious young people.");

define("WORK5_TITLE", "Tourist application with geolocalized notifications.");

//SLIDER 4
define("SLIDER4_TITLE", "Meet rudo");
define("SLIDER4_SUBTITLE", "A highly motivated and balanced team.");

define("TEAM_RICHARD_TITLE", "RICHARD MORLA");
define("TEAM_RICHARD_SUBTITLE", "CEO");
define("TEAM_RICHARD_TEXT", "Internacional MBA. More than 10 years of experience creating and running companies.");

define("TEAM_MARCOS_TITLE", "MARCOS PLAZAS");
define("TEAM_MARCOS_SUBTITLE", "DESIGNER");
define("TEAM_MARCOS_TEXT", "The creative guy. He have studied in Valencia and Hamburg.");

define("TEAM_FER_TITLE", "FERNANDO SALOM");
define("TEAM_FER_SUBTITLE", "IOS DEVELOPER");
define("TEAM_FER_TEXT", "Engineer. He is like a Swiss knife knowing how to develop in iOS, Android and Backend.");

define("TEAM_OSCAR_TITLE", "OSCAR VERA");
define("TEAM_OSCAR_SUBTITLE", "ANDROID DEVELOPER");
define("TEAM_OSCAR_TEXT", "The King of fragments, frameworks and  libraries. He loves google, but has recently changed to developing with iOS.");

define("TEAM_EMILIO_TITLE", "EMILIO CARRIÓN");
define("TEAM_EMILIO_SUBTITLE", "API & BACKEND");
define("TEAM_EMILIO_TEXT", "You call him by REST when you need something. Swift padawan and Django master.");

//SLIDER 5
define("SLIDER5_TITLE", "Contact");
define("SLIDER5_SUBTITLE", "Don't be afraid, ask us");

define("CONTACT_NAME", "Name");
define("CONTACT_EMAIL", "Email");
define("CONTACT_CONTENT", "Submit a request");
define("CONTACT_SUCCESS", "Thanks! We will answer you soon!");
define("CONTACT_BUTTON", "Send");
 
//SLIDER 6
define("SLIDER6_TITLE", "Ready to create<br/>
the next big thing?");

//SLIDER 7
define("SLIDER7_TITLE", "Our clients");
define("SLIDER7_SUBTITLE", "We work for big companies.");

//FOOTER
define("FOOTER_TITLE", "Contact");
define("FOOTER_SUBTITLE", "RUDO");

define("FOOTER_MAIL", "hola@rudo.es");
define("FOOTER_PHONE", "(+34) 654 217 299");
define("FOOTER_STREET", "Calle Sorní, nº7");
define("FOOTER_CITY", "Valencia, Spain. 46004");

define("FOOTER_OPTION1", "Home");
define("FOOTER_OPTION2", "Services");
define("FOOTER_OPTION3", "Projects");
define("FOOTER_OPTION4", "About");
define("FOOTER_OPTION5", "Submit a request");

//TEAM
define("RMBIO", "Lifelong entrepreneur, visionary, and modern renaissance man, Richard originally started Rudo in 2016 alongside Fernando and Oscar to create Sclusib, a social media network for influencers. Now, under Richard’s direction, Rudo is constantly growing its portfolio of projects and clientele. You can find Richard rocking SuperDry apparel (as he owns a franchise store here in Valencia) and running Spartan Races in his free time. 
Richard holds an International M.B.A  and has studied at Tonghi University & University of California Irvine. 
");
define("RMNAME", "Richard Morla");
define("RMJOB", "CEO");

define("FSBIO", "Creator, problem solver, and innovator, Fernando, as one of the Co-Founders is part of the backbone of this company. He commandeers not only his IOS team but the company as a unit with his Swiss Army Knife like repertoire of knowledge in not only IOS, but Android and Backend as well. 
Fernando holds an International M.B.A and has studied at the University of Madrid, Tongji, University of Shanghai, and the University of California Irvine. 
");
define("FSNAME", "Fernando Salom");
define("FSJOB", "CTO/Head of IOS Development");

define("OVBIO", "Our youngest Co-Founder and King of Fragments, Frameworks, Libraries. Oscar, the Head of Android Development Team, is a visionary and master in the Android programming industry. Known to sit on the Iron Throne of Rudo (funny because Oscar is the smallest man in the largest chair), Oscar is an Instagram enthusiast and programmably updates his followers with the brightest moments of his day.");
define("OVNAME", "Oscar Vera");
define("OVJOB", "Head of Android Development");

define("PVBIO", "Cinema enthusiast, Paella purist, and Ryan Reynold’s lookalike, Pablo is an all around people’s person and excels at building relationships on the fly. His expertise as an communicator gives him a unique approach to connecting with clientele. If he’s not at the movies catching up on the latest films, he’s at Mestalla Stadium cheering on Valencia CF. ");
define("PVNAME", "Pablo Villamayor");
define("PVJOB", "Head of Business Development");

define("ECBIO", "Swift Padawan and Django Master, Emilio, as the Head of API & Backend, thrives on mastering the newest technologies in the industry. His background in information engineering at ETSINF, UPV allow him to incorporate his experience in all his work.");
define("ECNAME", "Emilio Carrión");
define("ECJOB", "Head of API & Backend ");

define("KRBIO", "Master of Karate and Friendship, Keon is one of our intern’s from James Madison University in Harrisonburg, Virginia where he studies Media Arts & Design with a double minor in Business/Communication. He enjoys siestas at Playa de Marvarossa and has a philosophy to always try things at least once (like conejo - rabbit & caracoles - snails) ");
define("KRNAME", "Keon<br/> Rho");
define("KRJOB", "Business Development Intern ");

define("ACBIO", "Abby is one of our intern’s from James Madison University, located in Harrisonburg, Virginia. Abby is a Political Science student at JMU who is working with us for her study abroad program. When Abby isn’t with us in the office, she spends most of her time at the beach with her friends or exploring the city.");
define("ACNAME", "Abby Curtis");
define("ACJOB", "Business Development Intern ");

define("CGBIO", "Celia is part of our iOS Team at Rudo. Information engineering at ETSINF, UPV student, Celia easily complements her studies with her job. Discovering other cities, binge watching TV Shows and listening to music are some of her great hobbies in her free time.");
define("CGNAME", "Celia Gómez");
define("CGJOB", "Junior iOS software developer, student");

define("CLBIO", "Swift Rookie and part of the iOS Team at Rudo. Information engineering at ETSINF, UPV student. His role in Rudo is to work on Swift and to help making the apps. In his free time he enjoys learning and researching about programming languages, playing videogames and watching the latest films at the cinema.");
define("CLNAME", "Carlos López");
define("CLJOB", "Junior iOS software developer, student");

define("AOBIO", "Alvaro, as well as a being an iOS software developer here at Rudo, is a professional hanger outer outside the office. Alvaro is able to foster his hard work when the time comes to aid in the creation of our impeccable applications, while also studying at the University.");
define("AONAME", "Alvaro Ortí");
define("AOJOB", "IOS Software Developer ");

define("AFBIO", "A IOS software developer here at Rudo, Antonio is also studying multi-platform application development which allows him to help us keep up with the most up to technologies. Antonio has also worked for other development companies, which allows him to bring his experience to the development and creation here at Rudo.");
define("AFNAME", "Antonio Ferrando");
define("AFJOB", "IOS Software Developer ");

define("LBBIO", "A software developer on the Android Team. Laura, one of our two girls, is becoming a master in her field at a unprecedented rate. When Laura isn’t busy mastering her field, her heart belongs outdoors. She takes her person time hiking, biking or doing any outdoors activities that are available to her and her friends here in Valencia.");
define("LBNAME", "Laura Benitez");
define("LBJOB", "Android Software Developer ");

define("CSBIO", "An Android software developer, Java expert, and Siesta specialist, Carlos, when he’s not busy tackling code or his college schedule, likes to enjoy the simple things in life like taking great naps or spending quality time with his friends.");
define("CSNAME", "Carlos Sobrino");
define("CSJOB", "Android Software Developer ");
?>