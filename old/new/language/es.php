<?php
//MENU
define("MENU_OPTION1", "¿QUÉ HACEMOS?");
define("MENU_OPTION2", "PROYECTOS");
define("MENU_OPTION3", "EQUIPO");
define("MENU_OPTION4", "CONTACTAR");

//ANCHOR
define("ANCHOR_OPTION0", "inicio");
define("ANCHOR_OPTION1", "quehacemos");
define("ANCHOR_OPTION2", "proyectos");
define("ANCHOR_OPTION3", "equipo");
define("ANCHOR_OPTION4", "contactanos");

//SLIDER 1
define("SLIDER1_TITLE", "Diseño UX/UI & <br/>desarrollo de aplicaciones<br/>
	nativas en iOS y Android");

//SLIDER 2
define("SLIDER2_TITLE", "¿Qué hacemos?");
define("SLIDER2_SUBTITLE", "Somos inconformistas.");
define("SLIDER2_TEXT", "Buscamos el mejor diseño UI/UX y desarrollamos aplicaciones nativas, tanto en iOS como en Android. Encuentra la mejor solución tanto para validar tu MVP como para el desarrollo de tu gran empresa.");

define("SLIDER2_COL1_TITLE", "Diseño UI / UX");
define("SLIDER2_COL1_SUBTITLE", "Diseño centrado en la experiencia final de usuario, teniendo en cuenta a la marca y todas sus necesidades.");

define("SLIDER2_COL2_TITLE", "Desarrollo ANDROID & IOS");
define("SLIDER2_COL2_SUBTITLE", "Desarrollamos aplicaciones nativas en JAVA y SWIFT, para poder aprovechar el 100% del potencia de los dispositivos.");

define("SLIDER2_COL3_TITLE", "Desarrollo BACKEND");
define("SLIDER2_COL3_SUBTITLE", "La lógica de la aplicación y su alojamiento. Especialistas en creación de API Rest con Django y alojamiento cloud en Amazon Web Services.");

define("SLIDER2_BUTTON", "CONTACTAR");

//SLIDER 3
define("SLIDER3_TITLE", "Nuestros proyectos");
define("SLIDER3_SUBTITLE", "Somos rudos, pero nuestros resultados son impecables.");

//WORK1
define("WORK1_TITLE", "El mejor marketplace para comprar y vender material deportivo de segunda mano");

//WORK2
define("WORK2_TITLE", "Acércate a tus ídolos y gana premios. <br/>Monetiza tus fotos");

//WORK3
define("WORK3_TITLE", "Mereces estar siempre<br/>
online. Alquila tu <br/>
cargador y no te quedes<br/>
sin energía");

define("WORK4_TITLE", "Red social exclusiva para jóvenes ambiciosos.");

define("WORK5_TITLE", "Aplicación turística con notificaciones geolocalizadas.");

//SLIDER 4
define("SLIDER4_TITLE", "Conoce a RUDO");
define("SLIDER4_SUBTITLE", "Un equipo motivado y equilibrado. Una familia.");

define("TEAM_RICHARD_TITLE", "RICHARD MORLA");
define("TEAM_RICHARD_SUBTITLE", "DIRECTOR DE PROYECTOS");
define("TEAM_RICHARD_TEXT", "MBA Internacional, 10 años de experiencia en marketing de gran consumo y tres empresas en su mochila.");

define("TEAM_MARCOS_TITLE", "MARCOS PLAZAS");
define("TEAM_MARCOS_SUBTITLE", "DISEÑADOR UI/UX");
define("TEAM_MARCOS_TEXT", "El creativo. Formado en la Escuela de Arte Superior y Diseño en Valencia y en la Universidad HAW Hamburg.");

define("TEAM_FER_TITLE", "FERNANDO SALOM");
define("TEAM_FER_SUBTITLE", "IOS DEVELOPER");
define("TEAM_FER_TEXT", "Ingeniero Superior. Controla iOS, Android y Backend. Ha trabajado como freelance y en consultoras internacionales.");

define("TEAM_OSCAR_TITLE", "OSCAR VERA");
define("TEAM_OSCAR_SUBTITLE", "ANDROID DEVELOPER");
define("TEAM_OSCAR_TEXT", "El rey de los fragments, los frameworks y las librerias. Ama Google, pero acaba de pasarse a Mac.");

define("TEAM_EMILIO_TITLE", "EMILIO CARRIÓN");
define("TEAM_EMILIO_SUBTITLE", "API & BACKEND");
define("TEAM_EMILIO_TEXT", "Al que llamas via rest cuando necesitas algo. Swift padawan y Django master.");

//SLIDER 5
define("SLIDER5_TITLE", "Contacta con nosotros");
define("SLIDER5_SUBTITLE", "Pregúntanos o pide presupuesto. ¡Sin compromiso!");

define("CONTACT_NAME", "Nombre");
define("CONTACT_EMAIL", "Email");
define("CONTACT_CONTENT", "Pregúntanos lo que necesites o pide presupuesto sin ningún tipo de compromiso. ¡Nos pondremos en contacto contigo en seguida!");
define("CONTACT_SUCCESS", "Gracias!, pronto tendrás noticias nuestras");
define("CONTACT_BUTTON", "ENVIAR");
 
//SLIDER 6
define("SLIDER6_TITLE", "¡Atrévete a dar el salto <br/>
de calidad!");

//SLIDER 7
define("SLIDER7_TITLE", "Nuestros clientes");
define("SLIDER7_SUBTITLE", "Trabajamos con grandes empresas.");

//FOOTER
define("FOOTER_TITLE", "CONTACTA RUDO");
define("FOOTER_SUBTITLE", "RUDO");

define("FOOTER_MAIL", "hola@rudo.es");
define("FOOTER_PHONE", "(+34) 654 217 299");
define("FOOTER_STREET", "Calle Sorní, nº7");
define("FOOTER_CITY", "Valencia, España. 46004");

define("FOOTER_OPTION1", "Inicio");
define("FOOTER_OPTION2", "¿Qué hacemos?");
define("FOOTER_OPTION3", "Proyectos");
define("FOOTER_OPTION4", "El equipo");
define("FOOTER_OPTION5", "Pide Presupuesto");

//TEAM
define("RMBIO", "Emprendedor de nacimiento y visionario. El ejemplo perfecto del moderno hombre del renacimiento. Richard creó Rudo junto Fernando y Oscar para dar vida a su primer proyecto, Sclusib, una red social para influencers. Ahora, bajo su dirección, Rudo está en constante crecimiento en cuanto a proyectos y clientela. También puedes encontrarlo vistiendo de SuperDry (posee una tienda de la misma marca en Valencia) o corriendo Spartan Races en su tiempo libre. ");
define("RMNAME", "Richard Morla");
define("RMJOB", "CEO");

define("FSBIO", "Creador, “solucionador” de problemas y, sobretodo, innovador. Fernando, como uno de los co-fundadores de Rudo, es parte de la columna vertebral de la empresa. No solo es el jefe del Team iOS sino que une a la empresa como si de una navaja suiza se tratara mostrando todo su repertorio de conocimientos no solo en iOS si no también en Android y en el Back-End.");
define("FSNAME", "Fernando Salom");
define("FSJOB", "CTO/Head of IOS Development");

define("OVBIO", "Nuestro fundador más joven, Rey de los “fragments”, “frameworks”, “librerías” y jefe del Team Android. Oscar es un visionario y experto en la industria de la programación de Android. Famoso por sentarse en el Trono de Hierro de Rudo, Oscar es un entusiasta de Instagram y normalmente muestra a sus followers sus mejores momentos del día.");
define("OVNAME", "Oscar Vera");
define("OVJOB", "Head of Android Development");

define("PVBIO", "Un entusiasta del cine, purista de la paella y gemelo perdido de Ryan Reynolds. Pablo es una persona cercana y buena construyendo relaciones personales de forma eficaz. Su experiencia como comunicador le da una visión única para conectar con los clientes. Si no está en el cine disfrutando de los últimos estrenos, estará en Mestalla animando a su Valencia C.F.");
define("PVNAME", "Pablo Villamayor");
define("PVJOB", "Head of Business Development");

define("ECBIO", "Swift Padawan y Django Jedi. Emilio, como jefe de API & Backend, sigue desarrollando todas sus habilidades en controlar las nuevas tecnologías en la industria. Su “background” en Informática en la ETSINF, UPV le permite incorporar toda su experiencia en su trabajo.");
define("ECNAME", "Emilio Carrión");
define("ECJOB", "Head of API & Backend ");

define("KRBIO", "Maestro de Karate y amistoso. Keon es uno de nuestros becarios de la Universidad James Madison de Harrisonburg (Virginia, USA) donde estudia Media Arts & Design junto a Business/Communication. Disfruta de sus siestas en la Playa de la Malvarrosa y tiene como filosofía de vida probar todo al menos una vez (como el conejo o los caracoles)");
define("KRNAME", "Keon<br/> Rho");
define("KRJOB", "Business Development Intern ");

define("ACBIO", "Abby es una de nuestras becarias de la Universidad James Madison de Harrisonburg (Virgina, USA) donde estudia Political Science. Cuando no está con nosotros en la oficina pasa la mayor parte del tiempo en la playa con sus amigos o explorando la bonita ciudad de Valencia.");
define("ACNAME", "Abby Curtis");
define("ACJOB", "Business Development Intern ");

define("CGBIO", "Celia forma parte de nuestro Team iOS. Estudiante de Informática en la ETSINF, UPV, Celia complementa perfectamente sus estudios con su trabajo. Como hobbies podrían destacar descubrir otras ciudades, devorar series y escuchar música.");
define("CGNAME", "Celia Gómez");
define("CGJOB", "Junior iOS software developer, student");

define("CLBIO", "Aprendiz de Swift y parte del grupo IOS de Rudo. Estudiante de Ingeniería de Informática, que compagina los estudios con una formación adicional en Rudo. Mi papel en Rudo es aprender Swift y ayudar en la elaboración de aplicaciones. En su tiempo libre disfruta de aprender e investigar sobre lenguajes de programación, jugar a videojuegos y ver películas en formato legal.");
define("CLNAME", "Carlos López");
define("CLJOB", "Junior iOS software developer, student");

define("AOBIO", "Álvaro controla tan bien la fiesta como su función de iOS Software Developer en Rudo. De entre sus geniales distintas habilidades podríamos destacar su duro trabajo cuando llega el momento de crear aplicaciones impecables.");
define("AONAME", "Alvaro Ortí");
define("AOJOB", "IOS Software Developer ");

define("AFBIO", " iOS Software Developer en Rudo. Antonio se está especializando en Desarrollo de Aplicaciones Multiplataforma por lo que le permite ayudarnos a realizar las tareas de la mejor forma posible. Gracias a sus trabajos en otras empresas, nos aporta la experiencia necesaria para el desarrollo y la creación de aplicaciones aquí en Rudo.");
define("AFNAME", "Antonio Ferrando");
define("AFJOB", "IOS Software Developer ");

define("LBBIO", "Software Developer en el Android Team. Laura, una de nuestras dos compañeras, se está convirtiendo rápidamente en una experta en el equipo Android. Cuando no está ocupada, disfruta de diferentes actividades al aire libre como excursionismo, montar en bicicleta o cualquier otra actividad de las que puedan disponer ella y sus amigas para disfrutar del día.");
define("LBNAME", "Laura Benitez");
define("LBJOB", "Android Software Developer ");

define("CSBIO", "Android Software Developer, experto en Java y especialista en siestas. Cuando no está ocupado generando código le encanta vivir las cosas simples de la vida como disfrutar de grandes siestas o pasar tiempo de calidad con sus amigos.");
define("CSNAME", "Carlos Sobrino");
define("CSJOB", "Android Software Developer ");
?>
