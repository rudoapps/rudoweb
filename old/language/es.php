<?php
//MENU
define("MENU_OPTION1", "¿QUÉ HACEMOS?");
define("MENU_OPTION2", "PROYECTOS");
define("MENU_OPTION3", "EQUIPO");
define("MENU_OPTION4", "CONTACTAR");

//ANCHOR
define("ANCHOR_OPTION0", "inicio");
define("ANCHOR_OPTION1", "quehacemos");
define("ANCHOR_OPTION2", "proyectos");
define("ANCHOR_OPTION3", "equipo");
define("ANCHOR_OPTION4", "contactanos");

//SLIDER 1
define("SLIDER1_TITLE", "Diseño UX/UI & <br/>desarrollo de aplicaciones<br/>
	nativas en iOS y Android");

//SLIDER 2
define("SLIDER2_TITLE", "¿Qué hacemos?");
define("SLIDER2_SUBTITLE", "Somos inconformistas.");
define("SLIDER2_TEXT", "Buscamos el mejor diseño UI/UX y desarrollamos aplicaciones nativas, tanto en iOS como en Android. Encuentra la mejor solución tanto para validar tu MVP como para el desarrollo de tu gran empresa.");

define("SLIDER2_COL1_TITLE", "Diseño UI / UX");
define("SLIDER2_COL1_SUBTITLE", "Diseño centrado en la experiencia final de usuario, teniendo en cuenta a la marca y todas sus necesidades.");

define("SLIDER2_COL2_TITLE", "Desarrollo ANDROID & IOS");
define("SLIDER2_COL2_SUBTITLE", "Desarrollamos aplicaciones nativas en JAVA y SWIFT, para poder aprovechar el 100% del potencia de los dispositivos.");

define("SLIDER2_COL3_TITLE", "Desarrollo BACKEND");
define("SLIDER2_COL3_SUBTITLE", "La lógica de la aplicación y su alojamiento. Especialistas en creación de API Rest con Django y alojamiento cloud en Amazon Web Services.");

define("SLIDER2_BUTTON", "CONTACTAR");

//SLIDER 3
define("SLIDER3_TITLE", "Nuestros proyectos");
define("SLIDER3_SUBTITLE", "Somos rudos, pero nuestros resultados son impecables.");

//WORK1
define("WORK1_TITLE", "El mejor marketplace para comprar y vender material deportivo de segunda mano");

//WORK2
define("WORK2_TITLE", "Acércate a tus ídolos y gana premios. <br/>Monetiza tus fotos");

//WORK3
define("WORK3_TITLE", "Mereces estar siempre<br/>
online. Alquila tu <br/>
cargador y no te quedes<br/>
sin energía");

define("WORK4_TITLE", "Red social exclusiva para jóvenes ambiciosos.");

define("WORK5_TITLE", "Aplicación turística con notificaciones geolocalizadas.");

//SLIDER 4
define("SLIDER4_TITLE", "Conoce a RUDO");
define("SLIDER4_SUBTITLE", "Un equipo motivado y equilibrado. Una familia.");

define("TEAM_RICHARD_TITLE", "RICHARD MORLA");
define("TEAM_RICHARD_SUBTITLE", "DIRECTOR DE PROYECTOS");
define("TEAM_RICHARD_TEXT", "MBA Internacional, 10 años de experiencia en marketing de gran consumo y tres empresas en su mochila.");

define("TEAM_MARCOS_TITLE", "MARCOS PLAZAS");
define("TEAM_MARCOS_SUBTITLE", "DISEÑADOR UI/UX");
define("TEAM_MARCOS_TEXT", "El creativo. Formado en la Escuela de Arte Superior y Diseño en Valencia y en la Universidad HAW Hamburg.");

define("TEAM_FER_TITLE", "FERNANDO SALOM");
define("TEAM_FER_SUBTITLE", "IOS DEVELOPER");
define("TEAM_FER_TEXT", "Ingeniero Superior. Controla iOS, Android y Backend. Ha trabajado como freelance y en consultoras internacionales.");

define("TEAM_OSCAR_TITLE", "OSCAR VERA");
define("TEAM_OSCAR_SUBTITLE", "ANDROID DEVELOPER");
define("TEAM_OSCAR_TEXT", "El rey de los fragments, los frameworks y las librerias. Ama Google, pero acaba de pasarse a Mac.");

define("TEAM_EMILIO_TITLE", "EMILIO CARRIÓN");
define("TEAM_EMILIO_SUBTITLE", "API & BACKEND");
define("TEAM_EMILIO_TEXT", "Al que llamas via rest cuando necesitas algo. Swift padawan y Django master.");

//SLIDER 5
define("SLIDER5_TITLE", "Contacta con nosotros");
define("SLIDER5_SUBTITLE", "Pregúntanos o pide presupuesto. ¡Sin compromiso!");

define("CONTACT_NAME", "Nombre");
define("CONTACT_EMAIL", "Email");
define("CONTACT_CONTENT", "Pregúntanos lo que necesites o pide presupuesto sin ningún tipo de compromiso. ¡Nos pondremos en contacto contigo en seguida!");
define("CONTACT_SUCCESS", "Gracias!, pronto tendrás noticias nuestras");
define("CONTACT_BUTTON", "ENVIAR");
 
//SLIDER 6
define("SLIDER6_TITLE", "¡Atrévete a dar el salto <br/>
de calidad!");

//SLIDER 7
define("SLIDER7_TITLE", "Nuestros clientes");
define("SLIDER7_SUBTITLE", "Trabajamos con grandes empresas.");

//FOOTER
define("FOOTER_TITLE", "CONTACTA RUDO");
define("FOOTER_SUBTITLE", "RUDO");

define("FOOTER_MAIL", "hola@rudo.es");
define("FOOTER_PHONE", "(+34) 654 217 299");
define("FOOTER_STREET", "Calle Sorní, nº7");
define("FOOTER_CITY", "Valencia, España. 46004");

define("FOOTER_OPTION1", "Inicio");
define("FOOTER_OPTION2", "¿Qué hacemos?");
define("FOOTER_OPTION3", "Proyectos");
define("FOOTER_OPTION4", "El equipo");
define("FOOTER_OPTION5", "Pide Presupuesto");
?>
