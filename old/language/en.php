<?php
//MENU
define("MENU_OPTION1", "SERVICES");
define("MENU_OPTION2", "PROJECTS");
define("MENU_OPTION3", "ABOUT");
define("MENU_OPTION4", "CONTACT");

//ANCHOR
define("ANCHOR_OPTION0", "home");
define("ANCHOR_OPTION1", "services");
define("ANCHOR_OPTION2", "projects");
define("ANCHOR_OPTION3", "about");
define("ANCHOR_OPTION4", "contact");

//SLIDER 1
define("SLIDER1_TITLE", "Mobile app <br/>development <br/>
	for iOS and Android");

//SLIDER 2
define("SLIDER2_TITLE", "Services");
define("SLIDER2_SUBTITLE", "We love what me do");
define("SLIDER2_TEXT", "We design and develop highly polished iPhone & Android apps for startups and enterprise clients.");

define("SLIDER2_COL1_TITLE", "Design, UI and UX");
define("SLIDER2_COL1_SUBTITLE", "Our mobile app designers are focused on usability and aesthetics.");

define("SLIDER2_COL2_TITLE", "iOS and Android development");
define("SLIDER2_COL2_SUBTITLE", "We use Java and Swift in order to build high performance and scalable mobile apps.");

define("SLIDER2_COL3_TITLE", "Backend and Cloud");
define("SLIDER2_COL3_SUBTITLE", "We have experience using robust back-end technologies, such as Django, to generate REST Api. We use Amazon Web Services servers.");

define("SLIDER2_BUTTON", "CONTACT");

//SLIDER 3
define("SLIDER3_TITLE", "Projects");
define("SLIDER3_SUBTITLE", "We are rude, but our results are impeccable.");

//WORK1
define("WORK1_TITLE", "A marketplace to buy and sell second hand bikes");

//WORK2
define("WORK2_TITLE", "Get closer to your idols. <br/>Get money with your followers");

//WORK3
define("WORK3_TITLE", "Do not run out of energy. Rent your mobile charger everywhere.");

define("WORK4_TITLE", "Exclusive social network for ambitious young people.");

define("WORK5_TITLE", "Tourist application with geolocalized notifications.");

//SLIDER 4
define("SLIDER4_TITLE", "Meet rudo");
define("SLIDER4_SUBTITLE", "A highly motivated and balanced team.");

define("TEAM_RICHARD_TITLE", "RICHARD MORLA");
define("TEAM_RICHARD_SUBTITLE", "CEO");
define("TEAM_RICHARD_TEXT", "Internacional MBA. More than 10 years of experience creating and running companies.");

define("TEAM_MARCOS_TITLE", "MARCOS PLAZAS");
define("TEAM_MARCOS_SUBTITLE", "DESIGNER");
define("TEAM_MARCOS_TEXT", "The creative guy. He have studied in Valencia and Hamburg.");

define("TEAM_FER_TITLE", "FERNANDO SALOM");
define("TEAM_FER_SUBTITLE", "IOS DEVELOPER");
define("TEAM_FER_TEXT", "Engineer. He is like a Swiss knife knowing how to develop in iOS, Android and Backend.");

define("TEAM_OSCAR_TITLE", "OSCAR VERA");
define("TEAM_OSCAR_SUBTITLE", "ANDROID DEVELOPER");
define("TEAM_OSCAR_TEXT", "The King of fragments, frameworks and  libraries. He loves google, but has recently changed to developing with iOS.");

define("TEAM_EMILIO_TITLE", "EMILIO CARRIÓN");
define("TEAM_EMILIO_SUBTITLE", "API & BACKEND");
define("TEAM_EMILIO_TEXT", "You call him by REST when you need something. Swift padawan and Django master.");

//SLIDER 5
define("SLIDER5_TITLE", "Contact");
define("SLIDER5_SUBTITLE", "Don't be afraid, ask us");

define("CONTACT_NAME", "Name");
define("CONTACT_EMAIL", "Email");
define("CONTACT_CONTENT", "Submit a request");
define("CONTACT_SUCCESS", "Thanks! We will answer you soon!");
define("CONTACT_BUTTON", "Send");
 
//SLIDER 6
define("SLIDER6_TITLE", "Ready to create<br/>
the next big thing?");

//SLIDER 7
define("SLIDER7_TITLE", "Our clients");
define("SLIDER7_SUBTITLE", "We work for big companies.");

//FOOTER
define("FOOTER_TITLE", "Contact");
define("FOOTER_SUBTITLE", "RUDO");

define("FOOTER_MAIL", "hola@rudo.es");
define("FOOTER_PHONE", "(+34) 654 217 299");
define("FOOTER_STREET", "Calle Sorní, nº7");
define("FOOTER_CITY", "Valencia, Spain. 46004");

define("FOOTER_OPTION1", "Home");
define("FOOTER_OPTION2", "Services");
define("FOOTER_OPTION3", "Projects");
define("FOOTER_OPTION4", "About");
define("FOOTER_OPTION5", "Submit a request");
?>